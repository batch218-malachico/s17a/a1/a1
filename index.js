/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:
	function actorName(){
		let fullName = prompt("Jason Smith"); 
		let age = prompt("35 years old."); 
		let completeAddress = prompt("New York City.");

		console.log("Hello, " + fullName);
		console.log("You are " + age); 
		console.log("You live in " + completeAddress); 
	};

	actorName();



/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//second function here:

	function topFiveBands(){
		console.log("1. Secondhand Serenade");
		console.log("2. Red Jumpsuit Apparatus");
		console.log("3. My Chemical Romance")
		console.log("4. FM Static")
		console.log("5. Dashboard Confessional")
	}

	topFiveBands();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function fiveMovies(){
		let movie1 = prompt("Every child is a special"); 
		console.log("1. " + movie1)
		console.log("Rotten tomatoes rating: 90%")
		let movie2 = prompt("Alita")
		console.log("2. " + movie2)
		console.log("Rotten tomatoes rating: 90%")
		let movie3 = prompt("West point")
		console.log("3. " + movie3)
		console.log("Rotten tomatoes rating: 90%")
		let movie4 = prompt("The Nerve")
		console.log("4. " + movie4)
		console.log("Rotten tomatoes rating: 90%")
		let movie5 = prompt("Great Escape")
		console.log("5. " + movie5)
		console.log("Rotten tomatoes rating: 90%")

	};

	fiveMovies();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();

function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);